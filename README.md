# Saradomin - C# Launcher for 2009Scape

Saradomin is an alternative launcher to the mainline java-based 2009Scape launcher that offers some additional features and streamlines configuration options.
## Download latest release

Windows: [Self Contained](https://gitlab.com/MattSG/Saradomin-Launcher/-/jobs/artifacts/main/raw/Saradomin/2009scape-launcher-sc.exe?job=pack_windows_sc)
